package com.nespresso.exercise.waiter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Restaurant {
	Map<String, ArrayList<String>> orders;
	int tableSize;

	public int initTable(int sizeOfTable) {
		try {
			orders = new HashMap<String, ArrayList<String>>();
			tableSize = sizeOfTable;
			return sizeOfTable * 4;
		} catch (Exception e) {
			throw new UnsupportedOperationException();
		}
	}

	public void customerSays(int tableId, String message) {
		try {
			String request[] = message.split(":");
			ArrayList<String> orderMessages = null;
			if (request.length == 2 && request[0] != null && !request[0].isEmpty() && request[1] != null
					&& !request[1].isEmpty()) {
				Set<String> names = this.orders.keySet();
				if (this.orders != null & this.orders.containsKey(request[0])) {
					orderMessages = new ArrayList<String>();
					for (String name : names) {
						if (name != null & name.equals(request[0]))
							orderMessages.addAll(this.orders.get(request[0]));
					}
					orderMessages.add(request[1]);
					this.orders.put(request[0], orderMessages);
				} else {
					orderMessages = new ArrayList<String>();
					orderMessages.add(request[1]);

					this.orders.put(request[0], orderMessages);
				}

			}

		} catch (Exception e) {
			throw new UnsupportedOperationException();
		}
	}

	public String createOrder(int tableId) {
		try {
			if (orders != null && orders.size() != tableSize) {
				return "MISSING " + (this.tableSize - orders.size());
			}
			if (orders != null && orders.size() > 0) {
				Set<String> megs = new HashSet<String>();
				for (String name : this.orders.keySet()) {
					megs.addAll(orders.get(name));
				}
			}
			String orderStr = "";
			for (String str : orders.keySet()) {
				orderStr = orderStr + "," + (orders.get(str).get(orders.get(str).size() - 1));
			}
			return orderStr.substring(2, orderStr.length());
		} catch (Exception e) {
			throw new UnsupportedOperationException();
		}
	}
}
